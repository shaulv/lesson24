﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Homework
{
    class MyLibrary
    {

        #region Properties
        Dictionary<string, Book> books = new Dictionary<string, Book>();
        #endregion

        #region Constructors
        public MyLibrary()
        {

        }
        #endregion

        #region Methods
        public bool AddBook(Book book)
        {
            if (HaveThisBook(book.Title))
                return false;

            books.Add(book.Title, book);
            return true;
        }
        public bool RemoveBook(string title)
        {
            if (!HaveThisBook(title))
                return false;

            books.Remove(title);
            return true;
        }
        public bool HaveThisBook(string title)
        {
            return books.ContainsKey(title);
        }
        public Book GetBook(string title)
        {
            if (HaveThisBook(title))
                return books[title];

            return null;
        }
        public Book GetBookByAuthor(string author)
        {
            Dictionary<string, Book> booksByAuthor = new Dictionary<string, Book>();

            foreach(var book in books)
            {
                booksByAuthor.Add(book.Value.Author, book.Value);
            }

            if (booksByAuthor.ContainsKey(author))
                return booksByAuthor[author];

            return null;
        }
        public void Clear()
        {
            books.Clear();
        }
        public List<string> GetAuthors()
        {
            List<string> authors = new List<string>();

            foreach(var book in books)
                authors.Add(book.Value.Author);

            return authors;
        }
        public List<Book> GetBooksSortedByAuthorName()
        {
            List<Book> booksList = books.Values.ToList();
            return booksList.OrderBy(cur => cur.Author).ToList();
        }
        public List<string> GetBookTitleSorted()
        {
            List<string> titles = new List<string>();

            foreach (var book in books)
                titles.Add(book.Value.Title);

            titles.Sort();

            return titles;
        }
        public int Count()
        {
            return books.Count;
        }
        public override string ToString()
        {
            string all = "";

            foreach (var book in books)
                all += book.ToString();

            return all;
        }
        #endregion

    }
}
