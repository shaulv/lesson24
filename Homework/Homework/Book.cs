﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework
{
    class Book
    {

        #region Properties
        public string Title { get; private set; }
        public string Content { get; private set; }
        public string Author { get; private set; }
        public string Category { get; private set; }
        #endregion

        #region Constructors
        public Book(string title, string content, string author, string category)
        {
            this.Title = title;
            this.Content = content;
            this.Author = author;
            this.Category = category;
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return $"Title: {this.Title}\nContent: {this.Content}\nAuthor: {this.Author}\nCategory: {this.Category}\n";
        }
        #endregion

    }
}
